﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GradeSubmitter
{
    class ConfigHandler
    {

        static readonly string PasswordHash = "P@@Sw0rd";
        static readonly string SaltKey = "S@LT&KEY";
        static readonly string VIKey = "@1B2c3D4e5F6g7H8";
        public string uname = null ; 
        public string pass = null ; 

        private XmlDocument xdoc = null ;
        private XmlElement config =null ;
        public string status
        {
            get;
            set;
        }
        private string filename = null;
        //private XmlTextReader reader ;

        public void CreateElement(string tagname , string text , bool encript = false )
        {
            XmlElement tag= xdoc.CreateElement(tagname);
            XmlText tagtext= xdoc.CreateTextNode( encript ? Encrypt( text ) : text );
            tag.AppendChild(tagtext);
            config.AppendChild(tag);
        }

        public void Save()
        {
            xdoc.Save("config.xml");
            this.status= "Saved ";
        }

        public ConfigHandler()
        {
            try
            {
                xdoc = new XmlDocument();
                config = xdoc.CreateElement("config.xml");
                xdoc.AppendChild(config);
            }
            catch (Exception ex)
            {
                status = "Problem Saving " + ex.ToString();
            }
        }


        public ConfigHandler(string filename )
        {
            this.filename = filename;
 
            try
            {
                //to chk file exist 
                XmlTextReader reader  = new XmlTextReader(filename);
            }
            catch (Exception ex)
            {
                // if not exist then create a blank one 
                xdoc = new XmlDocument();
                config = xdoc.CreateElement("config.xml");
                xdoc.AppendChild(config);
                this.Save();

            }
        }

        public string getElementByTagName( string tagname , bool dec =false  )
        {
            XmlTextReader reader = new XmlTextReader(filename);
            while (reader.Read()) 
            {
                if (reader.NodeType == XmlNodeType.Element && reader.Name == tagname) 
                {
                    while (reader.Read())
                    {
                        if( reader.NodeType == XmlNodeType.Text )
                            return dec ? Decrypt(reader.Value) : reader.Value;
                    }
                }
                
                    
            }
            return "";

        }


        public static string Encrypt(string plainText)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }

        public static string Decrypt(string encryptedText)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }
    }
}
