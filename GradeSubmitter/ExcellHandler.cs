﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeSubmitter
{
    class ExcellHandler 
    {

        private static Microsoft.Office.Interop.Excel.Application appExcel;
        private static Workbook newWorkbook = null;
        private static _Worksheet objsheet = null;


        public ExcellHandler(string path)
        {
            
            //excel_close();

            appExcel = new Microsoft.Office.Interop.Excel.Application();

            if (System.IO.File.Exists(path))
            {
                // then go and load this into excel
                newWorkbook = appExcel.Workbooks.Open(path, true, true);
                objsheet = (_Worksheet)appExcel.ActiveWorkbook.ActiveSheet;
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Unable to open file!");
                System.Runtime.InteropServices.Marshal.ReleaseComObject(appExcel);
                appExcel = null;
                System.Windows.Forms.Application.Exit();
            }
         
        }

        


        //Method to initialize opening Excel

        //Method to get value; cellname is A1,A2, or B1,B2 etc...in excel.
        string excel_getValue(string cellname)
        {
            string value = string.Empty;
            try
            {
                value = objsheet.get_Range(cellname).get_Value().ToString();
            }
            catch
            {
                value = "";
            }

            return value;
        }

        //Method to close excel connection
        static void excel_close()
        {
            if (appExcel != null)
            {
                try
                {
                    newWorkbook.Close();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(appExcel);
                    appExcel = null;
                    objsheet = null;
                }
                catch (Exception ex)
                {
                    appExcel = null;
                    System.Windows.Forms.MessageBox.Show("Unable to release the Object " + ex.ToString());
                }
                finally
                {
                    GC.Collect();
                }
            }
        }

        public String getColumnNameFromIndex(int column)
        {
            column--;
            String col = Convert.ToString((char)('A' + (column % 26)));
            while (column >= 26)
            {
                column = (column / 26) - 1;
                col = Convert.ToString((char)('A' + (column % 26))) + col;
            }
            return col;
        }

        public String get_cell_name_from_cell_text(int rr, int cr, string text)
        {
            string cellName, cellVal;

            for (int i = 1; i < rr; i++)
            {
                for (int j = 1; j < cr; j++)
                {
                    cellName = getColumnNameFromIndex(j) + i;

                    cellVal = excel_getValue(cellName);
                    //allcellval = allcellval + "  " + cellVal;

                    if (text.Equals(cellVal.Trim()))
                    {
                        return cellName;
                    }

                }
            }
            return null;
        }

        public string get_mark_from_id(string id , string inp_cell_name)
        {
            string cellNo = get_cell_name_from_cell_text(50, 50, id);
            string marks = excel_getValue(inp_cell_name.Substring(0, 1) + cellNo.Substring(1));

            return marks;

        }

         ~ExcellHandler()
        {
            //System.Windows.Forms.MessageBox.Show("Destructor");
            excel_close();

        }


    }
}
