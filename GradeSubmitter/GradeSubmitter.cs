﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
//using Excel = Microsoft.Office.Interop.Excel;

namespace GradeSubmitter
{
    public partial class GradeSubmitter : Form
    {
        ExcellHandler resultExcell = null;
        public GradeSubmitter()
        {
            InitializeComponent();
            ConfigHandler conf = new ConfigHandler("config.xml");
            string durl = conf.getElementByTagName("url");
            if (durl != "")
                if (!durl.StartsWith("http://") && !durl.StartsWith("https://"))
                {
                    durl = "http://" + durl;
                    webBrowser1.Url = new Uri(durl )  ;
                }
                
        }
        
        private static bool loggedin = false;
        string id_cell_name, fileName, inp_cell_name;
        public bool isSumitting;

        private void btnfileopen_Click(object sender, EventArgs e)
        {
            
            try
            {
                
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    btngradeSubmit.Enabled = true;

                    fileName = openFileDialog1.FileName;
                    this.resultExcell = new ExcellHandler(fileName);

                    id_cell_name = this.resultExcell.get_cell_name_from_cell_text(50,
                                                            50,
                                                            !id_col_tb.Text.Equals("") ? id_col_tb.Text : "ID");

                    inp_cell_name =  this.resultExcell.get_cell_name_from_cell_text(50, 50, num_col_tb.Text);
                    //MessageBox.Show(id_cell_name + "   " + inp_cell_name );
                    this.lblstatus.Text = "id sell name " + id_cell_name + "   " + "input cell name " + inp_cell_name;
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                //excel_close();
                this.resultExcell = null;
                GC.Collect();

            }


        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (!loggedin)
            {
                ConfigHandler conf = new ConfigHandler("config.xml");
                HtmlElement id_inp_box = webBrowser1.Document.GetElementById("ctl00_ContentPlaceHolder1_ctlLogin1_txtLogin");
                id_inp_box.InnerText = conf.getElementByTagName("uname");
                HtmlElement pass_inp_box = webBrowser1.Document.GetElementById("ctl00_ContentPlaceHolder1_ctlLogin1_txtPassword");
                pass_inp_box.InnerText = conf.getElementByTagName("pass",true);
                HtmlElement login_btn = webBrowser1.Document.GetElementById("ctl00_ContentPlaceHolder1_ctlLogin1_btnLogin");
                login_btn.InvokeMember("click");
                loggedin = true;
            }
            else if (isSumitting == true)
            {
                submitAGrage();
            }
            else
            {
                this.resultExcell = null;
                GC.Collect();

            }


        }


        private int submitAGrage()
        {
            HtmlDocument doc = webBrowser1.Document;
            HtmlElement stdDrp = doc.GetElementById("ctl00_ContentPlaceHolder2_drpStudents");
            //while(true)

            try
            {
                if (stdDrp != null)
                {
                    foreach (HtmlElement child in stdDrp.Children)
                    {
                        //child.SetAttribute("selected", "false");
                        string str = child.GetAttribute("selected");

                        if (str.Trim().ToLower() == "true")
                        {
                            string selectedId = child.InnerText.Trim().Substring(0, 11).Trim();
                            string inp_marks = this.resultExcell.get_mark_from_id(selectedId, inp_cell_name);
                            HtmlElement markTxt = doc.GetElementById("ctl00_ContentPlaceHolder2_txtMark");
                            markTxt.InnerText = inp_marks;
                            this.lblstatus.Text = "Inserting marks \n" + child.InnerText.ToString() + " : " + inp_marks;
                            break;

                        }
                    }

                    HtmlElement nextBtn = doc.GetElementById("ctl00_ContentPlaceHolder2_btnNext");
                    if (nextBtn != null)
                        nextBtn.InvokeMember("click");
                    else
                    {
                        HtmlElement finBtn = doc.GetElementById("ctl00$ContentPlaceHolder2$btnFinish");
                        this.lblstatus.Text = "Finish submiting ";
                        finBtn.InvokeMember("click");
                    }

                }

            }
            catch (Exception ex)
            {
                lblstatus.Text = "Problem Submiting Grade " + ex.ToString();
            }

            //@TODO 
            return 1;//ok 
        }

        private void btnsubmit_Click(object sender, EventArgs e)
        {

            isSumitting = true;
            btngradeSubmit.Enabled = false;
            btncalcel.Enabled = true;
            submitAGrage();
        }

        

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void btncalcel_Click(object sender, EventArgs e)
        {

            isSumitting = false;
            btngradeSubmit.Enabled = true;
            btncalcel.Enabled = false;
            this.resultExcell = null;
            GC.Collect();


        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (checkuname.Checked && checkpass.Checked)
            {
                ConfigHandler conf = new ConfigHandler();
                if (checkuname.Checked)
                {
                    conf.CreateElement("uname", txtusername.Text.ToString());
                }
                if (checkpass.Checked)
                {
                    conf.CreateElement("pass", txtpass.Text.ToString(), true);
                }
                if (checkurl.Checked)
                {
                    conf.CreateElement("url", txturl.Text.ToString());
                }
                conf.Save();
                lblstatus.Text = conf.status;
            }
            else
            {
                MessageBox.Show("U Didnt chk any chk box");
            }
        }





        
    }
}
