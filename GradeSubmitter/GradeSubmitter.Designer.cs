﻿namespace GradeSubmitter
{
    partial class GradeSubmitter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnfileopen = new System.Windows.Forms.Button();
            this.id_col_tb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.num_col_tb = new System.Windows.Forms.TextBox();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.btngradeSubmit = new System.Windows.Forms.Button();
            this.lblstatus = new System.Windows.Forms.Label();
            this.btncalcel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtusername = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtpass = new System.Windows.Forms.TextBox();
            this.checkpass = new System.Windows.Forms.CheckBox();
            this.checkuname = new System.Windows.Forms.CheckBox();
            this.btnconfsave = new System.Windows.Forms.Button();
            this.txturl = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.checkurl = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "b";
            // 
            // btnfileopen
            // 
            this.btnfileopen.Location = new System.Drawing.Point(6, 117);
            this.btnfileopen.Margin = new System.Windows.Forms.Padding(4);
            this.btnfileopen.Name = "btnfileopen";
            this.btnfileopen.Size = new System.Drawing.Size(100, 28);
            this.btnfileopen.TabIndex = 0;
            this.btnfileopen.Text = "Open File";
            this.btnfileopen.UseVisualStyleBackColor = true;
            this.btnfileopen.Click += new System.EventHandler(this.btnfileopen_Click);
            // 
            // id_col_tb
            // 
            this.id_col_tb.Location = new System.Drawing.Point(7, 34);
            this.id_col_tb.Margin = new System.Windows.Forms.Padding(4);
            this.id_col_tb.Name = "id_col_tb";
            this.id_col_tb.Size = new System.Drawing.Size(132, 22);
            this.id_col_tb.TabIndex = 1;
            this.id_col_tb.Text = "ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "ID Column Text Ex:ID,Studen Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 64);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(290, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Number Column Text Ex: Quiz1,Quiz2,Writen";
            // 
            // num_col_tb
            // 
            this.num_col_tb.Location = new System.Drawing.Point(7, 87);
            this.num_col_tb.Margin = new System.Windows.Forms.Padding(4);
            this.num_col_tb.Name = "num_col_tb";
            this.num_col_tb.Size = new System.Drawing.Size(132, 22);
            this.num_col_tb.TabIndex = 4;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(16, 18);
            this.webBrowser1.Margin = new System.Windows.Forms.Padding(4);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(27, 25);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(732, 335);
            this.webBrowser1.TabIndex = 5;
            this.webBrowser1.Url = new System.Uri("http://www.aiub.edu/app/login.aspx", System.UriKind.Absolute);
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // btngradeSubmit
            // 
            this.btngradeSubmit.Enabled = false;
            this.btngradeSubmit.Location = new System.Drawing.Point(6, 164);
            this.btngradeSubmit.Margin = new System.Windows.Forms.Padding(4);
            this.btngradeSubmit.Name = "btngradeSubmit";
            this.btngradeSubmit.Size = new System.Drawing.Size(100, 28);
            this.btngradeSubmit.TabIndex = 6;
            this.btngradeSubmit.Text = "Start Submit";
            this.btngradeSubmit.UseVisualStyleBackColor = true;
            this.btngradeSubmit.Click += new System.EventHandler(this.btnsubmit_Click);
            // 
            // lblstatus
            // 
            this.lblstatus.AutoSize = true;
            this.lblstatus.ForeColor = System.Drawing.Color.Red;
            this.lblstatus.Location = new System.Drawing.Point(13, 368);
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(46, 17);
            this.lblstatus.TabIndex = 7;
            this.lblstatus.Text = "status";
            // 
            // btncalcel
            // 
            this.btncalcel.Enabled = false;
            this.btncalcel.Location = new System.Drawing.Point(6, 211);
            this.btncalcel.Name = "btncalcel";
            this.btncalcel.Size = new System.Drawing.Size(101, 25);
            this.btncalcel.TabIndex = 8;
            this.btncalcel.Text = "Calcel";
            this.btncalcel.UseVisualStyleBackColor = true;
            this.btncalcel.Click += new System.EventHandler(this.btncalcel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "User Name";
            // 
            // txtusername
            // 
            this.txtusername.Location = new System.Drawing.Point(100, 20);
            this.txtusername.Name = "txtusername";
            this.txtusername.Size = new System.Drawing.Size(156, 22);
            this.txtusername.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "Password";
            // 
            // txtpass
            // 
            this.txtpass.Location = new System.Drawing.Point(100, 48);
            this.txtpass.Name = "txtpass";
            this.txtpass.PasswordChar = '#';
            this.txtpass.Size = new System.Drawing.Size(156, 22);
            this.txtpass.TabIndex = 12;
            // 
            // checkpass
            // 
            this.checkpass.AutoSize = true;
            this.checkpass.Location = new System.Drawing.Point(100, 126);
            this.checkpass.Name = "checkpass";
            this.checkpass.Size = new System.Drawing.Size(197, 21);
            this.checkpass.TabIndex = 13;
            this.checkpass.Text = "Save Password(Encripted)";
            this.checkpass.UseVisualStyleBackColor = true;
            // 
            // checkuname
            // 
            this.checkuname.AutoSize = true;
            this.checkuname.Location = new System.Drawing.Point(100, 99);
            this.checkuname.Name = "checkuname";
            this.checkuname.Size = new System.Drawing.Size(137, 21);
            this.checkuname.TabIndex = 14;
            this.checkuname.Text = "Save User Name";
            this.checkuname.UseVisualStyleBackColor = true;
            // 
            // btnconfsave
            // 
            this.btnconfsave.Location = new System.Drawing.Point(100, 197);
            this.btnconfsave.Name = "btnconfsave";
            this.btnconfsave.Size = new System.Drawing.Size(75, 23);
            this.btnconfsave.TabIndex = 15;
            this.btnconfsave.Text = "Save";
            this.btnconfsave.UseVisualStyleBackColor = true;
            this.btnconfsave.Click += new System.EventHandler(this.button1_Click);
            // 
            // txturl
            // 
            this.txturl.Location = new System.Drawing.Point(100, 76);
            this.txturl.Name = "txturl";
            this.txturl.Size = new System.Drawing.Size(190, 22);
            this.txturl.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 17);
            this.label5.TabIndex = 17;
            this.label5.Text = "Url";
            // 
            // checkurl
            // 
            this.checkurl.AutoSize = true;
            this.checkurl.Location = new System.Drawing.Point(100, 153);
            this.checkurl.Name = "checkurl";
            this.checkurl.Size = new System.Drawing.Size(222, 38);
            this.checkurl.TabIndex = 18;
            this.checkurl.Text = "make url default\r\n(Ex 172.16.2.1/app/login.aspx)";
            this.checkurl.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(755, 33);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(352, 286);
            this.tabControl1.TabIndex = 19;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.btncalcel);
            this.tabPage1.Controls.Add(this.btnfileopen);
            this.tabPage1.Controls.Add(this.id_col_tb);
            this.tabPage1.Controls.Add(this.btngradeSubmit);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.num_col_tb);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(344, 257);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Grade Submit";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtusername);
            this.tabPage2.Controls.Add(this.checkurl);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.txturl);
            this.tabPage2.Controls.Add(this.txtpass);
            this.tabPage2.Controls.Add(this.btnconfsave);
            this.tabPage2.Controls.Add(this.checkpass);
            this.tabPage2.Controls.Add(this.checkuname);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(344, 257);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Config";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // GradeSubmitter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 445);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblstatus);
            this.Controls.Add(this.webBrowser1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "GradeSubmitter";
            this.Text = "Grade Sumbitter";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnfileopen;
        private System.Windows.Forms.TextBox id_col_tb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox num_col_tb;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Button btngradeSubmit;
        private System.Windows.Forms.Label lblstatus;
        private System.Windows.Forms.Button btncalcel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtusername;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtpass;
        private System.Windows.Forms.CheckBox checkpass;
        private System.Windows.Forms.CheckBox checkuname;
        private System.Windows.Forms.Button btnconfsave;
        private System.Windows.Forms.TextBox txturl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkurl;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;

    }
}

